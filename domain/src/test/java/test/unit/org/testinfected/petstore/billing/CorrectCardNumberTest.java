package test.unit.org.testinfected.petstore.billing;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.CreditCardBuilder.aVisa;
import static test.support.org.testinfected.petstore.builders.CreditCardBuilder.aCreditCard;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;

import org.junit.Test;
import org.testinfected.petstore.billing.CreditCardType;

public class CorrectCardNumberTest {

	// ----- Visa -----

	/**
	 * For this project, we use validations instead of throw statement. Due to
	 * that, we use the function assertThat(...). The first parameter is a
	 * message to describe what validates the assert function. The second
	 * parameter validationOf(...) pass the object that we need to do the test.
	 * The thirst parameter is the "match" that we need to validate. In this
	 * example, when we run the case we expect a violation because the visa
	 * number is not correct.
	 * 
	 * Also remember: For this exercise use for object creation the different
	 * builder classes.
	 */
	@Test
	public void isInvalidVisaCaseBase() {
		assertThat("validation of visa with invalid number due to begins with 5 instead of 4",
				validationOf(aVisa().withNumber("5111222233334444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	/**
	 * This case is a little bit different. We pass the function succeeds(),
	 * this function expect to not have any violation during the execution.
	 */
	@Test
	public void isInvalidVisaCaseGood() {
		assertThat("validation of visa with valid number", validationOf(aVisa().withNumber("4111222233334444")),
				succeeds());
	}

	@Test
	public void isInvalidVisaCaseGreaterString() {
		assertThat("validation of visa with invalid number due to has more than 16 digits",
				validationOf(aVisa().withNumber("41112222333344440")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidVisaCaseSmallerString() {
		assertThat("validation of visa with invalid number due to has less than 16 digits",
				validationOf(aVisa().withNumber("411122223333444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidVisaCaseNull() {
		assertThat("validation of visa with invalid number due to has null value",
				validationOf(aVisa().withNumber(null)), violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidVisaCaseNoNumbers() {
		assertThat("validation of visa with invalid number due to it is not a string of digits",
				validationOf(aVisa().withNumber("Paquito el chocolatero")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	// ----- Mastercard -----

	@Test
	public void isInvalidMastercardCaseBase() {
		assertThat("validation of Mastercard with invalid number due to begins with 4 instead of 5",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("4111222233334444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidMastercardCaseGood() {
		assertThat("validation of Mastercard with valid number",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("5111222233334444")),
				succeeds());
	}

	@Test
	public void isInvalidMastercardCaseGreaterString() {
		assertThat("validation of Mastercard with invalid number due to has more than 16 digits",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("51112222333344440")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidMastercardCaseSmallerString() {
		assertThat("validation of Mastercard with invalid number due to has less than 16 digits",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("511122223333444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidMastercardCaseNull() {
		assertThat("validation of Mastercard with invalid number due to has null value",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber(null)),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidMastercardCaseNoNumbers() {
		assertThat("validation of Mastercard with invalid number due to it is not a string of digits",
				validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("Paquito el chocolatero")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	// ----- American Express -----

	@Test
	public void isInvalidAmericanExpressCaseBase() {
		assertThat("validation of American Express with invalid number due to begins with 4 instead of 3",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("4111222233334444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidAmericanExpressCaseGood() {
		assertThat("validation of American Express with valid number",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("341222233334444")), succeeds());
	}

	@Test
	public void isInvalidAmericanExpressCaseGreaterString() {
		assertThat("validation of American Express with invalid number due to has more than 16 digits",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("3412222333344444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidAmericanExpressCaseSmallerString() {
		assertThat("validation of American Express with invalid number due to has less than 16 digits",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("34122223333444")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidAmericanExpressCaseNull() {
		assertThat("validation of American Express with invalid number due to has null value",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber(null)),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

	@Test
	public void isInvalidAmericanExpressCaseNoNumbers() {
		assertThat("validation of American Express with invalid number due to it is not a string of digits",
				validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("Paquito el chocolatero")),
				violates(on("cardNumber"), withMessage("incorrect")));
	}

}
