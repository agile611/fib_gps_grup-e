package test.unit.org.testinfected.petstore.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;

import org.junit.Test;

public class ItemTest {

    @Test
    public void UniqueIDNumber() {
        assertThat("The identifier number of the item cannot be repeated", 
        		!anItem().withNumber("46545").build().equals(anItem().withNumber("46546").build()));
    }
}

