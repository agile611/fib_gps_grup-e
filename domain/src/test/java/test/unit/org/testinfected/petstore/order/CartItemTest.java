package test.unit.org.testinfected.petstore.order;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;
import static test.support.org.testinfected.petstore.builders.CartBuilder.aCart;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.testinfected.petstore.order.CartItem;
import org.testinfected.petstore.product.Item;

public class CartItemTest {
	
	
	@Test public void checkOneItemInCart() {
		CartItem cartItem = new CartItem(anItem().build());
        assertThat("validation one element in cart returns one element", cartItem.getQuantity(), is(1));
    }
	
	@Test public void checkSumValueItems() {
		CartItem cartItem = new CartItem(anItem().priced("5").build());
		cartItem.incrementQuantity();
		assertThat("validation sum of values is correct", cartItem.getTotalPrice().intValue(), is(10));
    }
	
	@Test public void checkDescriptionItem(){
		CartItem cartItem = new CartItem(anItem().describedAs("Description").build());
		assertThat("validation item's description returned correctly",cartItem.getItemDescription(), is("Description"));
	}
	
	@Test public void checkPriceUpdate(){
		CartItem cartItem = new CartItem(anItem().priced("5").build());
		assertThat("validation value update managed correctly", cartItem.getTotalPrice().intValue(), is(5));
		//Changing total value
		cartItem.incrementQuantity();
		assertThat("validation value update managed correctly", cartItem.getTotalPrice().intValue(), is(10));
	}
	
	
	
}
