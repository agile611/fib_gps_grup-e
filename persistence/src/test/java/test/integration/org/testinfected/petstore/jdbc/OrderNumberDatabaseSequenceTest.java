package test.integration.org.testinfected.petstore.jdbc;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.ItemsDatabase;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.OrderNumberDatabaseSequence;
import org.testinfected.petstore.db.ProductsDatabase;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.product.DuplicateItemException;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.ItemNumber;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.product.ProductCatalog;
import org.testinfected.petstore.transaction.QueryUnitOfWork;
import org.testinfected.petstore.transaction.Transactor;
import test.support.org.testinfected.petstore.builders.Builder;
import test.support.org.testinfected.petstore.builders.ItemBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.testinfected.petstore.db.Access.idOf;
import static org.testinfected.petstore.db.Access.productOf;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.a;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;
import static test.support.org.testinfected.petstore.jdbc.HasFieldWithValue.hasField;

public class OrderNumberDatabaseSequenceTest {
	
	/*Hem de crear una connexió amb la base de dades de test*/
    Database database = Database.test();
    Connection connection = database.connect();
    OrderNumberDatabaseSequence onds;
    
    
    
    /*Un mètode de Set Up amb l’annotation @Before que prepara la base de dades*/
    @Before public void
    setUp() throws SQLException {
    	/*Hem de crear un objecte OrderNumberDatabaseSequence amb la connexió corresponent i amb aquest objecte ja podem fer el test*/
    	onds = new OrderNumberDatabaseSequence(connection);
    }
    
    /*Un mètode de Tear Down amb l’annotation @After que tanca la connexió amb la base de dades*/
    @After public void
    tearDown() throws SQLException {
        connection.close();
    }
    
    /*Sense utilitzar matcher*/
    @Test public void
    autoincrementOrderNumber() throws Exception {
    	
    	OrderNumber orderNumber = onds.nextOrderNumber();
    	OrderNumber nextOrderNumber = onds.nextOrderNumber();
    	
    	int first = Integer.parseInt(orderNumber.getNumber());
    	int next = Integer.parseInt(nextOrderNumber.getNumber());
    	
        assertThat("next number sequece", first + 1 == next);
    }
    
    /*Utilitzant un Matcher*/
    @Test public void
    autoincrementOrderNumberMatcher() throws Exception {
    	
    	OrderNumber orderNumber = onds.nextOrderNumber();
    	OrderNumber nextOrderNumber = onds.nextOrderNumber();
    	
    	int next = Integer.parseInt(nextOrderNumber.getNumber());
    	
        assertThat("next number sequence", orderNumber, nextOrderNumber(next));
    }
    
    
	private Matcher<OrderNumber> nextOrderNumber(final int number) {
        return new FeatureMatcher<OrderNumber, Integer>(equalTo(number), "has number", "number") {
            protected Integer featureValueOf(OrderNumber actual) {
                return Integer.parseInt(actual.getNumber())+1;
            }
        };
    }
}
