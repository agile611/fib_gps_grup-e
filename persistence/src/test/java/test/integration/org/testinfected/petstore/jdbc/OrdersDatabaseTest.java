package test.integration.org.testinfected.petstore.jdbc;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.OrdersDatabase;
import org.testinfected.petstore.order.Cart;
import org.testinfected.petstore.order.LineItem;
import org.testinfected.petstore.order.Order;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.Product;

import test.support.org.testinfected.petstore.builders.CartBuilder;
import test.support.org.testinfected.petstore.builders.ItemBuilder;
import test.support.org.testinfected.petstore.builders.OrderBuilder;
import test.support.org.testinfected.petstore.builders.ProductBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

public class OrdersDatabaseTest {

	// DB set-up connection
    Database database = Database.test();
    Connection connection = database.connect();
    Order newOrder;
	OrdersDatabase orderDatabase;
    
	@Before
	public void setUp() {
		newOrder = OrderBuilder.anOrder().build();
		orderDatabase = new OrdersDatabase(connection);
		orderDatabase.record(newOrder);
	}
	
    @After
    public void tearDown() throws SQLException {
    	connection.close();
    }
	
	
	@Test
	public void findByIdTestCase() {
		// Steps:
		// Put order into the db
		String orderNumberValue = newOrder.getNumber();
		// Get order from db by id
		Order orderFromDb = orderDatabase.find(new OrderNumber(orderNumberValue));
		// Assert that it is the correct order (i.e not null value and the same OrderNumber).
		assertNotNull(orderFromDb);
		assertEquals(newOrder.getNumber(), orderFromDb.getNumber());
	}
	
	/**
	 * This test asserts that after add two items of the same product to the cart
	 * the order is not corrupted. For us, the definition of "corrupted" is when
	 * the order has the correct number of order's items (if we put 2 items, so will be 2)
	 * and the items of the same product are inside the order.
	 */
	@Test
	public void orderWithMoreThanOneElementCase() {
		Product product = ProductBuilder.aProduct("batamanta").build();
		// Two items with the same product.
		Item item1 = ItemBuilder.a(product).build(); 
		Item item2 = ItemBuilder.a(product).build(); 
		item1.getProductNumber(); 
		Cart cart = CartBuilder.aCart().containing(item1, item2).build();
		newOrder = OrderBuilder.anOrder().from(cart).build();
		assertNotNull(newOrder);
		List<LineItem> items = newOrder.getLineItems();
		assertEquals(2, items.size());
		// We assume that the order has the items ordered by time.
		// So that the first item that we put to the cart is the first element
		// of the list.
		assertEquals(item1.getNumber(), items.get(0).getItemNumber());
		assertEquals(item2.getNumber(), items.get(1).getItemNumber());
	}
	
	

}
